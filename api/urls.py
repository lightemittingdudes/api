from django.conf.urls import url, include
from rest_framework import routers
from api import views

router = routers.DefaultRouter()
router.register(r'schedule', views.ScheduleViewSet)
router.register(r'outletgroup', views.OutletGroupViewSet)
router.register(r'outlet', views.OutletViewSet)
router.register(r'sensortype', views.SensorTypeViewSet)
router.register(r'sensor', views.SensorViewSet)
#router.register(r'temperaturedata', views.TemperatureDataViewSet)
#router.register(r'humiditydata', views.HumidityDataViewSet)
#router.register(r'co2data', views.CO2DataViewSet)
urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]
