from django.contrib import admin
from .models import Outlet,OutletGroup,Schedule,SensorType,Sensor

admin.site.register(Outlet)
admin.site.register(OutletGroup)
admin.site.register(Schedule)
admin.site.register(SensorType)
admin.site.register(Sensor)

# Register your models here.
