from rest_framework import serializers
from api.models import Schedule,OutletGroup,Outlet,SensorType,Sensor,TemperatureData,HumidityData,CO2Data

class ScheduleSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Schedule
#	fields = ('description','on_time','off_time','off_duration','on_duration','date_on','frequency','type')

class OutletGroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
	model = OutletGroup

class OutletSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Outlet

class SensorTypeSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = SensorType

class SensorSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Sensor

class TemperatureDataSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = TemperatureData

class HumidityDataSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = HumidityData

class CO2DataSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = CO2Data
