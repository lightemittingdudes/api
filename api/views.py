#from django.shortcuts import render

# Create your views here.
from api.models import Schedule,OutletGroup,Outlet,SensorType,Sensor,TemperatureData,HumidityData,CO2Data
from api.serializers import ScheduleSerializer,OutletGroupSerializer,OutletSerializer,SensorTypeSerializer,SensorSerializer,TemperatureDataSerializer,HumidityDataSerializer,CO2DataSerializer
from rest_framework import viewsets

class ScheduleViewSet(viewsets.ModelViewSet):
    queryset = Schedule.objects.all().order_by('-on_time')
    serializer_class = ScheduleSerializer

class OutletGroupViewSet(viewsets.ModelViewSet):
    queryset = OutletGroup.objects.all().order_by('-schedule')
    serializer_class = OutletGroupSerializer

class OutletViewSet(viewsets.ModelViewSet):
    queryset = Outlet.objects.all().order_by('-description')
    serializer_class = OutletSerializer

class SensorTypeViewSet(viewsets.ModelViewSet):
    queryset = SensorType.objects.all().order_by('-description')
    serializer_class = SensorTypeSerializer
#    lookup_field = 'description'

class SensorViewSet(viewsets.ModelViewSet):
    queryset = Sensor.objects.all().order_by('-port')
    serializer_class = SensorSerializer

#class TemperatureDataViewSet(viewsets.ModelViewSet):
#    queryset = TemperatureData.objects.all().order_by('-')
#    serializer_class = TemperatureDataSerializer

#class HumidityDataViewSet(viewsets.ModelViewSet):
#    queryset = HumidityData.objects.all().order_by('-')
#    serializer_class = HumidityDataSerializer

#class CO2DataViewSet(viewsets.ModelViewSet):
#    queryset = CO2Data.objects.all().order_by('-')
#    serializer_class = CO2DataSerializer

