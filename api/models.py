from django.db import models

# Create your models here.

class Schedule(models.Model):
    description = models.CharField(max_length=100)
#    api_root = 
    on_time = models.DateTimeField(blank=True, null=True)
    off_time = models.DateTimeField(blank=True, null=True)
    off_duration = models.IntegerField(blank=True, null=True)
    on_duration = models.IntegerField(blank=True, null=True)
    date_on = models.DateTimeField(blank=True, null=True)
    TYPE_CHOICES = (
        ('fixed', 'Fixed'),
        ('interval', 'Interval')
    )
    FREQ_CHOICES = (
        ('once', 'Once'),
        ('recur', 'Recurring')
    )
    #frequency probably needs to be changed to something else
    #a way to implement what days you want the schedule to run
    #eg Mondays and Wednesdays only
    frequency = models.CharField(max_length=5, choices=FREQ_CHOICES)
    type = models.CharField(max_length=8, choices=TYPE_CHOICES)
    
    def __str__(self):
        return '%s %s' % (self.description, self.type)

class OutletGroup(models.Model):
    description = models.CharField(max_length=100)
    schedule = models.ForeignKey(Schedule, blank=True, null=True)
    outlets = models.ManyToManyField('Outlet', blank=True)
    
    def __str__(self):
        return self.description

class Outlet(models.Model):
    description = models.CharField(max_length=100)
    pin = models.IntegerField()
    #group = models.ForeignKey(OutletGroup, blank=True, null=True)
    #group = models.ManyToManyField(OutletGroup, blank=True)
    last_on = models.DateTimeField(blank=True, null=True)
    #schedules = models.ManyToManyField(Schedule)
    schedule = models.ForeignKey(Schedule, blank=True, null=True)
    
    def __str__(self):
        return self.description

class SensorType(models.Model):
    name = models.CharField(max_length=30)
    description = models.CharField(max_length=100)
    platform = models.CharField(max_length=15)
    plugin = models.CharField(max_length=100, blank=True, null=True)

    def __str__(self):
        return '%s %s' % (self.name, self.description)

class Sensor(models.Model):
    port = models.CharField(max_length=100, blank=True, null=True)
    description = models.CharField(max_length=100)
    type = models.ForeignKey(SensorType, blank=True, null=False)
    uom = models.CharField(max_length=20)

    def __str__(self):
        return self.description
    
class TemperatureData(models.Model):
    sensor = models.ForeignKey(Sensor)
    value = models.IntegerField()
    time = models.DateTimeField()
    uom = models.CharField(max_length=20)

class HumidityData(models.Model):
    sensor = models.ForeignKey(Sensor)
    value = models.IntegerField()
    time = models.DateTimeField()
    uom = models.CharField(max_length=20)

class CO2Data(models.Model):
    sensor = models.ForeignKey(Sensor)
    value = models.IntegerField()
    time = models.DateTimeField()
    uom = models.CharField(max_length=20)
